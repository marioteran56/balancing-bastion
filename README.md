# Practica de balanceo de cargas de alta disponibilidad con Ansible

Esta práctica consiste en crear 5 diferentes servidores (bastion, servidor web 1, servidor web 2, servidor web 3 y servidor frontal) dentro de contenedores con una imagen Debian y para el servidor frontal una imagen de NGINX. 

La idea es que el **servidor frontal** sea el encargado de recibir las peticiones de los clientes y redirigirlas a los **servidores web**, los cuales cuentan por dentro con una serie instancias de node contenidas dentro de contenedores con una imagen de Node, que a su vez tambien cuentan con un servidor frontal NGINX que redirige las peticiones a cada instancia de Node. 

Por otro lado, el **servidor Bastion** es el encargado de controlar los demás servidores mediante un playbook de Ansible el cual se encargará de instalar Docker y Git en los servidores web, para crear los contenedores con las imágenes de Node y NGINX por medio de la descarga del repositorio der Gitlab https://gitlab.com/marioteran56/balancing-app.git.

Podemos representar dicho concepto de mejor manera con el siguiente diagrama:

![Diagrama](./diagrama_balanceo_de_cargas.png)

El playbook de Ansible con el que se instala Docker se encuentra adjunto en el repositorio como [playbook.yml](playbook.yml).

## ¿Cómo funciona?

Esta práctica utiliza un archvio **[docker-compose](docker-compose.yml)** en donde se crean una serie de contenedores a partir de dos distintos Dockerfiles.

**[DockerfileBas](DockerfileBas)** se utiliza para la creación de una imagen Debian con los paquetes de SSH, Sudo, Ansible y Python, al igual que generamos las llaves publica y privada SSH. En este caso, creamos un contenedor llamado **bastion-server**, que simboliza nuestro Bastion en el cual mediante volúmenes insertamos dentro del mismo los archivos [playbook.yml](playbook.yml), [hosts](hosts), [remote-hosts](remote-hosts) (lista de los servidores remotos) y [servers](servers) (carpeta con archivos docker-compose.yml para indicar diferentes numeros de instancias por cada servidor web).

```dockerfile
  bastion-server:
    build: 
      context: .
      dockerfile: DockerfileBas
    container_name: bastion-server
    hostname: bastion-server
    tty: true
    volumes:
      - ./servers:/root/servers
      - ./remote-hosts.txt:/tmp/remote-hosts.txt
      - ./playbook.yml:/root/playbook.yml
      - ./hosts:/etc/ansible/hosts
```

**[DockerfileDef](DockerfileDef)** se utiliza para la creación de una imagen Debian con los paquetes de SSH, Sudo y Python. En este caso, creamos 3 contenedores llamados **web-server-1**, **web-server-2** y **web-server-3** que simboliza nuestros otros servidores a los cuales accederemos mediante el Bastion Host mediante SSH.

```dockerfile
  web-server-1:
    build: 
      context: .
      dockerfile: DockerfileDef
    container_name: web-server-1
    hostname: web-server-1
    privileged: true
    tty: true
  web-server-2:
    build: 
      context: .
      dockerfile: DockerfileDef
    container_name: web-server-2
    hostname: web-server-2
    privileged: true
    tty: true
  web-server-3:
    build: 
      context: .
      dockerfile: DockerfileDef
    container_name: web-server-3
    hostname: web-server-3
    privileged: true
    tty: true
```

Por ultimo, creamos un contenedor llamado **server-balancer** que simboliza nuestro servidor frontal con una imagen de NGINX.

```dockerfile
  server-balancer:
    image: nginx
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    ports:
      - 80:80
    depends_on:
      - web-server-1
      - web-server-2
      - web-server-3
```

Cabe destacar que en el archivo [nginx.conf](nginx.conf) se encuentra la configuración de NGINX para que pueda redirigir las peticiones a los distintos servidores web por medio del puerto 80, en lugar de los puertos 3000, 3001 y 3002, como era el caso de la práctica anterior.

```nginx
  http {
    upstream app {
      server web-server-1:80;
      server web-server-2:80;
      server web-server-3:80;
    }
    ...
  }
```

## Uso

Para poder ejecutar correctamente los contenedores, primeramente ejecutamos dentro de nuestro proyecto el siguiente comando (tomando en cuenta que tenemos instalado **docker compose**): 

```bash
docker compose up -d
```

Seguido de esto, para que nuestro servidor Bastion pueda conectarse correctamente a los demás servidores(contenedores) debemos copiar la llave publica de nuestro Bastion a los demás servidores. Para esto, ejecutamos el siguiente comando:

```bash
docker cp bastion-server:/root/.ssh/id_rsa.pub /tmp/id_rsa.pub
```

Donde estamos guardando nuestra llave pública en un archivo temporal dentro de nuestro ordenador. Luego tenemos que transferir dicho archivo a nuestros otros servidores, para esto ejecutamos los siguientes comandos:

```bash
docker exec -i web-server-1 sh -c 'cat > /root/.ssh/authorized_keys' < /tmp/id_rsa.pub
docker exec -i web-server-2 sh -c 'cat > /root/.ssh/authorized_keys' < /tmp/id_rsa.pub
docker exec -i web-server-3 sh -c 'cat > /root/.ssh/authorized_keys' < /tmp/id_rsa.pub
```

Una vez hecho esto, agregamos la huella digital de las llaves SSH de nuestros servidores remotos en nuestro servidor Bastion, para esto ejecutamos el siguiente comando:

```bash
docker exec -it bastion-server sh -c 'ssh-keyscan -f /tmp/remote-hosts.txt >> /root/.ssh/known_hosts'
```

No olvidemos que en esta práctica, nos estaremos conectando a nuestro contenedor Bastion mediante SSH, por lo que debemos agregar nuestra llave pública a nuestro servidor Bastion, para esto ejecutamos el siguiente comando:

```bash
docker exec -i bastion-server sh -c 'cat > /root/.ssh/authorized_keys' < ~/.ssh/id_rsa.pub
```

Finalmente, para poder ejecutar nuestro playbook y que Ansible se conecte a los demás servidores, no conectamos a nuestro servidor Bastion mediante SSH, ejecutando el siguiente comando:

```bash
ssh root@0.0.0.0 -p 2222
```

Una vez dentro de nuestro servidor Bastion, ejecutamos el siguiente comando para que Ansible se conecte a los demás servidores y ejecute el playbook:

```bash
ansible-playbook playbook.yml
```